package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entities.Ruche;
import com.example.demo.entities.Rucher;
import com.example.demo.entities.User;
import com.example.demo.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
    private UserRepository UserRepository;

    public UserController() {
    }

    public UserController(UserRepository UserRepository) {
        this.UserRepository = UserRepository;
    }

    @GetMapping("/all")
    public List<User> getAll(){
    List<User> Users=this.UserRepository.findAll();
    return Users;
    }
    
    @GetMapping("/usernames")
    public List<String> getAllRuchers(){
    List<User> Users=this.UserRepository.findAll();
    List<String> names = new ArrayList<>();
    for(User e : Users) {
    	names.add(e.getUsername());
    }
    return names;
    }

    @RequestMapping(value="/ruchers/{username}", method=RequestMethod.GET,produces = "application/json")
    public List<Rucher> findRucherByUsername(@PathVariable("username") String username){
    	List<User> users = this.UserRepository.findAll();
    	System.out.println("username :"+ username );
    	List<Rucher> ruchers= new ArrayList<>();		
    	for(User u : users) {
    		if(u.getUsername().equals(username))
    			ruchers=u.getListeRuchers();
    	}
    	return ruchers;
    }	
    
    @RequestMapping(value="/ruches/{idRucher}", method=RequestMethod.GET,produces = "application/json")
    public List<Ruche> findRuchesByIdRucher(@PathVariable("idRucher") String idRucher){
   
    	List<Rucher> ruchers= new ArrayList<>();		
    	List<Ruche> ruches= new ArrayList<>();
    	for(Rucher r : ruchers) {
    		if(r.getId().equals(idRucher))
    			ruches=r.getListeRuches();
    	}
    	return ruches;
    }	
    
    @GetMapping("/username/{username}")
    public List<User> getByUsername(@PathVariable("username") String username){
    	List<User> users=this.UserRepository.findByUsername(username);
    	return users;
    }

    @PutMapping
    public void update(@RequestBody User User){
        this.UserRepository.save(User);
    }

    @PostMapping
    public void insert(@RequestBody User User){
        this.UserRepository.insert(User);
    }
  
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id){
      this.UserRepository.deleteById(id);
    }
    
    @GetMapping("/{id}")
    public Optional<User> getById(@PathVariable("id") String id){
        Optional<User> User = this.UserRepository.findById(id);
        return User;
    }
   
    @RequestMapping(value="/login", method=RequestMethod.POST)
    public Boolean checkLogin(@RequestBody User u ){
       
    	User u1 = this.UserRepository.findUserByUsername(u.getUsername());
        System.out.println("user :" + u);
        if(u.getPwd().equals(u1.getPwd()))
        	return true;
        else
        	return false;
    }


}
