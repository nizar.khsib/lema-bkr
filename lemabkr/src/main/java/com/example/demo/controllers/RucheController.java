package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entities.Capture;
import com.example.demo.entities.Note;
import com.example.demo.entities.Ruche;
import com.example.demo.repositories.RucheRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RestController
@RequestMapping("/ruches")
public class RucheController {
	@Autowired
    private RucheRepository RucheRepository;

    public RucheController() {
    }

    public RucheController(RucheRepository RucheRepository) {
        this.RucheRepository = RucheRepository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces={"application/json"})
    public List<Ruche> getAll(){
    List<Ruche> Ruches=this.RucheRepository.findAll();
    return Ruches;
    }
    
    @PostMapping
    public void insert(@RequestBody Ruche Ruche){
        this.RucheRepository.insert(Ruche);
    }

    @PutMapping
    public void update(@RequestBody Ruche Ruche){
        this.RucheRepository.save(Ruche);
    }

    @DeleteMapping("/{id}")

    public void delete(@PathVariable("id") String id){
      this.RucheRepository.deleteById(id);
    }

    @GetMapping("/notes/{id}")
    public List<Note> getNoteRucheById(@PathVariable("id") String id){
    	List<Ruche> ruches =  this.RucheRepository.findAll();
    	List<Note> notes= new ArrayList<>();
    	for(Ruche r : ruches) {
    		if(r.getId().equals(id)) {
    			notes=r.getNotesRuches();
    		}
    	}
        return notes;
    }
    
    @GetMapping("/captures/{id}")
    public List<Capture> getCaptureRucheById(@PathVariable("id") String id){
    	List<Ruche> ruches =  this.RucheRepository.findAll();
    	List<Capture> captures= new ArrayList<>();
    	for(Ruche r : ruches) {
    		if(r.getId().equals(id)) {
    			captures=r.getCapturesRuches();
    		}
    	}
        return captures;
    }
 
    //update notes to develop
    
    @GetMapping("/{id}")
    public Optional<Ruche> getRucheById(@PathVariable("id") String id){
        Optional<Ruche> Ruche = this.RucheRepository.findById(id);
        return Ruche;
    }
}
