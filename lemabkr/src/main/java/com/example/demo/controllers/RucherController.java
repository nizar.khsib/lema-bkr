package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entities.Rucher;
import com.example.demo.repositories.RucherRepository;

import java.util.List;


@Service
@RestController
@RequestMapping("/ruchers")
public class RucherController {

	@Autowired
    private RucherRepository RucherRepository;

    public RucherController() {
    }

    public RucherController(RucherRepository RucherRepository) {
        this.RucherRepository = RucherRepository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces={"application/json"})
    public List<Rucher> getAll(){
    List<Rucher> Ruchers=this.RucherRepository.findAll();
    return Ruchers;
    }

    @PutMapping
    public void insert(@RequestBody Rucher Rucher){
        this.RucherRepository.insert(Rucher);
    }
    
    @PostMapping
    public void update(@RequestBody Rucher Rucher){
        this.RucherRepository.save(Rucher);
    }
    
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id){
      this.RucherRepository.deleteById(id);
    }

 /*
    @GetMapping
    public Rucher getById(@PathVariable("id") String id){
        Rucher Rucher = this.RucherRepository.findById(id);
        return Rucher;
    }*/

    /*
    @GetMapping("/price/{maxPrice}")
    public List<Rucher> getByPricePerNight(@PathVariable("maxPrice") int maxPrice){
        return this.RucherRepository.findByPricePerNight(maxPrice);
    }*/

}
