package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.demo.entities.Capture;
import com.example.demo.entities.Note;
import com.example.demo.entities.Ruche;
import com.example.demo.entities.Rucher;
import com.example.demo.entities.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.CaptureRepository;
import com.example.demo.repositories.NoteRepository;
import com.example.demo.repositories.RucheRepository;
import com.example.demo.repositories.RucherRepository;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner{
   
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RucheRepository rucheRepository;
	@Autowired
	private RucherRepository rucherRepository;
	@Autowired
	private NoteRepository noteRepository;
	@Autowired
	private CaptureRepository captureRepository;
	
    public DbSeeder() {
    }

    public DbSeeder(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

    	this.userRepository.deleteAll();
    	this.rucheRepository.deleteAll();
    	this.rucherRepository.deleteAll();
    	this.noteRepository.deleteAll();
    	
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    	
    	
    	
    	//affectation des ruches à un rucher
    //	rucher1.listeRuches=Arrays.asList(ruche3);
    	
    	
    	//final List<Ruche> listeRuches1 = Arrays.asList(ruche1);
    	//final List<Rucher> listeRuchers1 = Arrays.asList(rucher1);
    	final Capture capture1 = new Capture(null,null,11.4f,11.6f,15.6f,df.parse("12/03/2018 14:00"));
    	final Capture capture2 = new Capture(null,null,11.4f,11.6f,15.6f,df.parse("12/03/2018 15:00"));
    	final Capture capture3 = new Capture(null,null,11.4f,11.6f,15.6f,df.parse("12/03/2018 16:00"));
    	final List<Capture> listeCaptures= Arrays.asList(capture1,capture2,capture3);
    	final Rucher rucher1 = new Rucher("xAskqpo",11.14f,16.1524f,null,null,null);
    	//final List<Rucher> listeRuchers1= Arrays.asList(rucher1);
    	final User u1 = new User(null,"jcp","apis123",df.parse("12/03/2018 00:00"),null);
    	final Note n1 = new Note(null,"Ajout d'une hausse",df.parse("20/03/2018 00:00"));
    	final Note n2 = new Note(null,"Ajout de sirop",df.parse("15/03/2018 00:00"));
    	final List<Note> listeNotes= Arrays.asList(n1,n2);
    	final Ruche ruche1 = new Ruche(null,"ruche1",0,0,0,listeNotes, listeCaptures,u1,rucher1);
    	//final List<Ruche> listeRuches= Arrays.asList(ruche1);
    	//rucher1.setListeRuches(Arrays.asList(ruche1));
		User u2 = new User();
		u2.setUsername("blg");
		u2.setPwd("apis123");
		u2.setCreatedAt(df.parse("12/03/2018 00:00"));
		
		
		User u3 = new User();
		u3.setUsername("clo");
		u3.setPwd("apis123");
		u3.setCreatedAt(df.parse("12/03/2018 00:00"));
		
		
		User u4 = new User();
		u4.setUsername("jhe");
		u4.setPwd("apis123");
		u4.setCreatedAt(df.parse("12/03/2018 00:00"));
	//	this.userRepository.deleteAll();
		
		
		List<User> users = Arrays.asList(u1,u2,u3,u4);
		
		System.out.println(users);
		this.noteRepository.save(n1);
		this.rucheRepository.save(ruche1);
		this.rucherRepository.save(rucher1);
	    this.userRepository.saveAll(users);
	   

    }
}
