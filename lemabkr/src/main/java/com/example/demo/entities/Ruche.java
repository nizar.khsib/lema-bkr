package com.example.demo.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Ruche")
public class Ruche {
	@Id
	public String id;
	public String tag;
	public double poidsJour;
	public double poidsSemaine;
	public double poidsMois;
	public List<Note> notesRuches;
	public List<Capture> capturesRuches;
	public User user;
	public Rucher rucher;
	
	
	public Ruche() {
		super();
	}
	public Ruche(String id, String tag, double poidsJour, double poidsSemaine, double poidsMois, List<Note> notesRuches,
			List<Capture> capturesRuches, User user, Rucher rucher) {
		super();
		this.id = id;
		this.tag = tag;
		this.poidsJour = poidsJour;
		this.poidsSemaine = poidsSemaine;
		this.poidsMois = poidsMois;
		this.notesRuches = notesRuches;
		this.capturesRuches = capturesRuches;
		this.user = user;
		this.rucher = rucher;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public double getPoidsJour() {
		return poidsJour;
	}
	public void setPoidsJour(double poidsJour) {
		this.poidsJour = poidsJour;
	}
	public double getPoidsSemaine() {
		return poidsSemaine;
	}
	public void setPoidsSemaine(double poidsSemaine) {
		this.poidsSemaine = poidsSemaine;
	}
	public double getPoidsMois() {
		return poidsMois;
	}
	public void setPoidsMois(double poidsMois) {
		this.poidsMois = poidsMois;
	}
	public List<Note> getNotesRuches() {
		return notesRuches;
	}
	public void setNotesRuches(List<Note> notesRuches) {
		this.notesRuches = notesRuches;
	}
	public List<Capture> getCapturesRuches() {
		return capturesRuches;
	}
	public void setCapturesRuches(List<Capture> capturesRuches) {
		this.capturesRuches = capturesRuches;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Rucher getRucher() {
		return rucher;
	}
	public void setRucher(Rucher rucher) {
		this.rucher = rucher;
	}
	
	
	
	
		
	
}
