package com.example.demo.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "Rucher")
public class Rucher {
	@Id
	public String id;
	public double longitude;
	public double latitude;
	public User user;
	public List<Ruche> listeRuches;
	public List<Note> notesRuchers;
	
	public Rucher() {
		super();
	}
	public Rucher(String id, double longitude, double latitude, User user, List<Ruche> listeRuches,
			List<Note> notesRuchers) {
		super();
		this.id = id;
		this.longitude = longitude;
		this.latitude = latitude;
		this.user = user;
		this.listeRuches = listeRuches;
		this.notesRuchers = notesRuchers;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Ruche> getListeRuches() {
		return listeRuches;
	}
	public void setListeRuches(List<Ruche> listeRuches) {
		this.listeRuches = listeRuches;
	}
	public List<Note> getNotesRuchers() {
		return notesRuchers;
	}
	public void setNotesRuchers(List<Note> notesRuchers) {
		this.notesRuchers = notesRuchers;
	}
	
	
}
