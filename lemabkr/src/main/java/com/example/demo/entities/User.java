package com.example.demo.entities;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "User")
public class User {
	@Id
	public String id;
	public String username;
	public String pwd;
	public Date CreatedAt;
	public List<Rucher> listeRuchers;
	
	public User() {
		super();
	}

	public User(String id, String username, String pwd, Date createdAt, List<Rucher> listeRuchers) {
		super();
		this.id = id;
		this.username = username;
		this.pwd = pwd;
		CreatedAt = createdAt;
		this.listeRuchers = listeRuchers;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Date getCreatedAt() {
		return CreatedAt;
	}

	public void setCreatedAt(Date createdAt) {
		CreatedAt = createdAt;
	}

	public List<Rucher> getListeRuchers() {
		return listeRuchers;
	}

	public void setListeRuchers(List<Rucher> listeRuchers) {
		this.listeRuchers = listeRuchers;
	}
	
	 
}
