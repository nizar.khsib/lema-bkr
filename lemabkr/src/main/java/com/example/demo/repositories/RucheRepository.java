package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Note;
import com.example.demo.entities.Ruche;

@Service
@Repository
public interface RucheRepository extends MongoRepository<Ruche,String> {

	@Query(value = "{Ruche.id:?0}")
	List<Note> findNotesByRucheId(String idRuche);
}
